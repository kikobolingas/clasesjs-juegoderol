# clasesJS-JuegodeRol

Crea dos personajes en función de su raza (humano, elfo, enano o mediano) y su clase (guerrero, mago o pícaro) generando aleatóriamente sus atributos de fuerza, constitución, destreza, inteligencia, sabiduría, carisma, vitalidad y energía, en función de la raza y clase elegidas por el usuario.

Tras generar las fichas permite el enfrentamiento entre ellas, calculando el daño del ataque lanzado y el gasto de energia que ese ataque causa.

Si la energia llega a 0 no permite lanzar más ataques, pero puedes defenderte, lo cual permite incrementar la energia y reducir el daño recivido en los ataques del rival.