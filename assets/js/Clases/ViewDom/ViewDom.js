import imagenDerrotado from '../../../img/derrotado.jpg';

export default class ViewDom {
    /* 
    *   Clase: Esta clase es la encargada de renderizar los elementos en el DOM.
    */
    constructor(){
        this.whereGoing = document.querySelector('#cajaFichas');
    }

    paintInitialCards(pjId){
        /* 
        *   M�todo(p�blico)
        *
        *   Recive
        *       pjId (integer): el id del personaje.
        *
        *   Realiza
        *       Clona el template para pintar la tarjeta donde se configurar� cada personaje.
        *       
        */
        
        let card = document.querySelector('template').content.cloneNode(true);
        card.querySelector("section").setAttribute('id', 'card_'+pjId);
        card.querySelector(".cajaBotonEnviar button").setAttribute('id', 'genera_'+pjId);
        card.querySelector(".cajaBotonAtacar button.ataque").setAttribute('id', 'ataca_'+pjId);
        card.querySelector(".cajaBotonAtacar button.defensa").setAttribute('id', 'defend_'+pjId);
        this.whereGoing.appendChild(card);
    }

    paintPj(pj, pjId){
        /* 
        *   M�todo(p�blico)
        *
        *   Recive
        *       pj (object): el objeto del personaje.
        *       pjId (integer): el id del personaje.
        *
        *   Realiza
        *       Crea la tarjeta del personaje en funci�n de las estad�sticas generadas y la imagen en funci�n de la raza y la clase.
        *       
        */
        let card = document.querySelector('section#card_'+pjId);
        card.querySelector(".selecionRaza").style.display="none";
        card.querySelector(".selecionClase").style.display="none";
        card.querySelector(".cajaBotonEnviar").style.display="none";
        card.querySelector(".datosFicha").style.display="block";
        card.querySelector(".vitalidad").style.display="block";
        card.querySelector(".energia").style.display="block";
        card.querySelector(".cajaBotonAtacar").style.display="block";
        card.querySelector(".nombre").readOnly = true;
        card.querySelector("article").style.backgroundImage = 'url('+pj.image+')';
        card.querySelector(".datoFue").textContent = pj._str;
        card.querySelector(".datoCon").textContent = pj._con;
        card.querySelector(".datoDex").textContent = pj._dex;
        card.querySelector(".datoInt").textContent = pj._int;
        card.querySelector(".datoWis").textContent = pj._wis;
        card.querySelector(".datoCha").textContent = pj._cha;
        card.querySelector(".vitalidad progress").setAttribute('max', pj._vit);
        card.querySelector(".energia progress").setAttribute('max', pj._sta);
        card.querySelector(".vitalidad progress").setAttribute('value', pj._vit);
        card.querySelector(".energia progress").setAttribute('value', pj._sta);
    }

    pjUpdate(pj, pjId){
        /* 
        *   M�todo(p�blico)
        *
        *   Recive
        *       pj (object): el objeto del personaje.
        *       pjId (integer): el id del personaje.
        *
        *   Realiza
        *       Actualiza la tarjeta del personaje en funci�n de las estad�sticas guardadas tras un ataque o una defensa.
        *       
        */
        let card = document.querySelector('section#card_'+pjId);
        card.querySelector(".datoFue").textContent = pj._str;
        card.querySelector(".vitalidad progress").setAttribute('value', pj._vit);
        card.querySelector(".energia progress").setAttribute('value', pj._sta);

        if(pj._defending == true){
            card.querySelector(".contenedorShield").style.display="block";
        }else{
            card.querySelector(".contenedorShield").style.display="none";
        }

        if(pj._sta == 0){
            card.querySelector("#ataca_"+pjId).disabled = true;
            card.querySelector("#ataca_"+pjId).style.filter = "grayscale(100%) opacity(50%)";
        }else{
            card.querySelector("#ataca_"+pjId).disabled = false;
            card.querySelector("#ataca_"+pjId).style.filter = "grayscale(0%) opacity(100%)";
        }

        if(pj._vit == 0){
            card.style.filter = "grayscale(80%) opacity(90%)";
            card.querySelector(".contenedorShield").style.display="none";
            card.querySelector(".cajaBotonAtacar").style.display="none";
            card.querySelector("article").style.backgroundImage = 'url("' + imagenDerrotado + '")';
            card.querySelector(".datoFue").textContent = 0;
            card.querySelector(".datoCon").textContent = 0;
            card.querySelector(".datoDex").textContent = 0;
            card.querySelector(".datoInt").textContent = 0;
            card.querySelector(".datoWis").textContent = 0;
            card.querySelector(".datoCha").textContent = 0;
            card.querySelector(".energia progress").setAttribute('value', 0);
        }
    }
}