import Heroe from '../Heroe.js';

export default class Humano extends Heroe {
    constructor(pjName, pjRace, pjClass, pjAttributtes) {
        super(pjName, pjRace, pjClass, pjAttributtes);
        this._str = parseInt(this._str) - 1;
        this._con = parseInt(this._con) - 2;
        this._dex = parseInt(this._dex) + 2;
        this._wis = parseInt(this._wis) + 1;
        this._cha = parseInt(this._cha) + 2;
    }
}