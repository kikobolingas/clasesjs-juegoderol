import Mediano from '../Mediano.js';
import imagen from '../../../../../img/mediano_guerrero.jpg';

export default class GuerreroMediano extends Mediano{
    constructor(pjName, pjRace, pjClass, pjAttributtes) {
        super(pjName, pjRace, pjClass, pjAttributtes);
        this._str = parseInt(this._str) + 2;
        this._con = parseInt(this._con) + 2;
        this._dex = parseInt(this._dex) + 1;
        this._int = parseInt(this._int) - 1;
        this._wis = parseInt(this._wis) + 1;
        this._sta = parseInt(this._sta) + parseInt( parseInt (this._con) * 10);
        this.image = imagen;
    }
    launchAttack(){
        /* 
        *   Método(público)
        *
        *   Realiza
        *       Calcula el gasto de estamina y devuelve el daño que produce el ataque.
        *       
        */
        if(this._defending){ this._defending = false; }
        let stamExpense = parseInt(this._sta) - parseInt( parseInt (this._throwDicesd20.throw) + parseInt(this._con));
        if(stamExpense > 0){
            this._sta = stamExpense;
        }else{
            this._sta = 0;
        }
        let atackDamage = parseInt( parseInt (this._throwDicesd20.throw) + parseInt(this._str));
        return atackDamage
    }

    reciveAttack(damage){
        /* 
        *   Método(público)
        *
        *   Recive
        *       damage (integer): el daño recivido antes de aplicarle la reducción por defensa y clase.
        *
        *   Realiza
        *       Modifica la vit en función del ataque recivido.
        *       
        */

        let damageReceived =  parseInt(damage) - parseInt (this._throwDicesd20.throw);
        if(damageReceived < 0 ){ damageReceived = 0;}
        if(this._defending){
            if(damageReceived > 0 ){ damageReceived = parseInt(damageReceived) - parseInt(this._str);} 
        }
        damageReceived = parseInt(this._vit) - parseInt(damageReceived);
        if(damageReceived < 0 ){
            this._vit = 0;
        }else{
            this._vit = damageReceived;
        }
    }
}