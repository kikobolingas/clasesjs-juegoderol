import ThrowDices from '../ThrowDices/ThrowDices.js';

export default class Heroe {
    /* 
    *   Clase: (super) Se encarga de la gestión de todos los atributos y metodos de cada personaje.
    *
    *   Recibe:  
    *       pjName: (string) es el nombre del personaje.
    *       pjRace: (string) es la raza del personaje. 
    *       pjClass: (string) es la clase del personaje. 
    *       pjAttributtes: (objeto) contiene los atributos del personaje (str, con, dex, int, wis, cha, vit y sta). 
    * 
    */
    constructor(pjName, pjRace, pjClass, pjAttributtes) {
        this._name = pjName;
        this._race = pjRace;
        this._class = pjClass;
        this._str = pjAttributtes.str;
        this._con = pjAttributtes.con;
        this._dex = pjAttributtes.dex;
        this._int = pjAttributtes.int;
        this._wis = pjAttributtes.wis;
        this._cha = pjAttributtes.cha;
        this._vit = pjAttributtes.vit;
        this._sta = pjAttributtes.sta;
        this._defending = false;
        this._throwDicesd20  = new ThrowDices(20);
    }

    defend(){
        /* 
        *   M?todo(p?blico)
        *
        *   Realiza
        *       Actualiza la estamina y el estado de _defending.
        *       
        */
        this._defending = true;
        this._sta = parseInt(this._sta) + parseInt( this._throwDicesd20.throw ) + parseInt(this._con);
    }
}