import Humano from '../Humano.js';
import imagen from '../../../../../img/humano_mago.jpg';

export default class MagoHumano extends Humano{
    constructor(pjName, pjRace, pjClass, pjAttributtes) {
        super(pjName, pjRace, pjClass, pjAttributtes);
        this._str = parseInt(this._str) - 2;
        this._con = parseInt(this._con) - 2;
        this._int = parseInt(this._int) + 2;
        this._wis = parseInt(this._wis) + 2;
        this._cha = parseInt(this._cha) + 1;
        this._sta = parseInt(this._sta) + parseInt( parseInt (this._wis) * 10);
        this.image = imagen;
    }
    
    launchAttack(){
        /* 
        *   Método(público)
        *
        *   Realiza
        *       Calcula el gasto de estamina y devuelve el daño que produce el ataque.
        *       
        */
        if(this._defending){ this._defending = false; }
        let stamExpense = parseInt(this._sta) - parseInt( parseInt (this._throwDicesd20.throw) + parseInt(this._wis));
        if(stamExpense > 0){
            this._sta = stamExpense;
        }else{
            this._sta = 0;
        }
        let atackDamage = parseInt( parseInt (this._throwDicesd20.throw) + parseInt(this._int));
        return atackDamage
    }

    reciveAttack(damage){
        /* 
        *   Método(público)
        *
        *   Recive
        *       damage (integer): el daño recivido antes de aplicarle la reducción por defensa y clase.
        *
        *   Realiza
        *       Modifica la vit en función del ataque recivido.
        *       
        */

        let damageReceived =  parseInt(damage) - parseInt (this._throwDicesd20.throw);
        if(damageReceived < 0 ){ damageReceived = 0;}
        if(this._defending){
            if(damageReceived > 0 ){ damageReceived = parseInt(damageReceived) - parseInt(this._int);} 
        }
        damageReceived = parseInt(this._vit) - parseInt(damageReceived);
        if(damageReceived < 0 ){
            this._vit = 0;
        }else{
            this._vit = damageReceived;
        }
    }

    defend(){
        /* 
        *   Método(público) Modifica el defend de Heroe.
        *
        *   Realiza
        *       Actualiza la estamina y el estado de _defending.
        *       
        */
        this._defending = true;
        this._sta = parseInt(this._sta) + parseInt( this._throwDicesd20.throw ) + parseInt(this._wis);
    }
}