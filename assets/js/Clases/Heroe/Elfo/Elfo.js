import Heroe from '../Heroe.js';

export default class Elfo extends Heroe {
    constructor(pjName, pjRace, pjClass, pjAttributtes) {
        super(pjName, pjRace, pjClass, pjAttributtes);
        this._str = parseInt(this._str) - 1;
        this._con = parseInt(this._con) - 1;
        this._dex = parseInt(this._dex) + 1;
        this._int = parseInt(this._int) + 2;
        this._wis = parseInt(this._wis) + 2;
    }
}