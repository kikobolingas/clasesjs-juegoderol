import AttributeGenerator from '../AttributeGenerator/AttributeGenerator.js';
import GuerreroHumano from '../Heroe/Humano/GuerreroHumano/GuerreroHumano.js';
import MagoHumano from '../Heroe/Humano/MagoHumano/MagoHumano.js';
import PicaroHumano from '../Heroe/Humano/PicaroHumano/PicaroHumano.js';
import GuerreroElfo from '../Heroe/Elfo/GuerreroElfo/GuerreroElfo.js';
import MagoElfo from '../Heroe/Elfo/MagoElfo/MagoElfo.js';
import PicaroElfo from '../Heroe/Elfo/PicaroElfo/PicaroElfo.js';
import GuerreroEnano from '../Heroe/Enano/GuerreroEnano/GuerreroEnano.js';
import MagoEnano from '../Heroe/Enano/MagoEnano/MagoEnano.js';
import PicaroEnano from '../Heroe/Enano/PicaroEnano/PicaroEnano.js';
import GuerreroMediano from '../Heroe/Mediano/GuerreroMediano/GuerreroMediano.js';
import MagoMediano from '../Heroe/Mediano/MagoMediano/MagoMediano.js';
import PicaroMediano from '../Heroe/Mediano/PicaroMediano/PicaroMediano.js';

const CLASSES = {GuerreroHumano, MagoHumano, PicaroHumano, GuerreroElfo, MagoElfo, PicaroElfo, GuerreroEnano, MagoEnano, PicaroEnano, GuerreroMediano, MagoMediano, PicaroMediano};

export default class PjGenerator {
    /* 
    *   Clase: Esta clase es la encargada de generar el objeto del personaje.
    */
    constructor(card){
        this.card = card;
    }

    _factoria(pjRace, pjClass){
        let name = pjClass+pjRace;
        return CLASSES[name];
    }

    get generatePj(){
        let pj;
        const pjName = this.card.querySelector("input.nombre").value;
        let pjRace = this.card.querySelector(".selecionRaza select").value.toLowerCase();
        let pjClass = this.card.querySelector(".selecionClase select").value.toLowerCase();
        pjRace = pjRace.charAt(0).toUpperCase() + pjRace.slice(1);
        pjClass = pjClass.charAt(0).toUpperCase() + pjClass.slice(1);
        const pjType = this._factoria(pjRace, pjClass);
        const initialAttributes = new AttributeGenerator().pjAttributes;
        return new pjType(pjName, pjRace, pjClass, initialAttributes);
    }
}