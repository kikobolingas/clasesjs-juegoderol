import ViewDom from '../ViewDom/ViewDom.js';
import PjGenerator from '../PjGenerator/PjGenerator.js';

/* 
*   Clase: Esta clase es la encargada de lanzar todo el juego.
*/
export default class MainGestor {
    constructor (){
        let view = new ViewDom();
        let players = [];
        for (let i=0; i<2; i++){
            view.paintInitialCards(i);
            document.querySelector('#genera_'+i).onclick = function (e) {
                /* 
                *   EventListener (click del boton generar)
                *
                *   Realiza
                *       Crea la tarjeta del personaje
                *
                */
                const card = document.querySelector('section#card_'+i);
                if(!card.querySelector("input.nombre").value==""){
                    e.preventDefault();
                    players[i]=new PjGenerator(card).generatePj;
                    view.paintPj(players[i], i);
                }
                e.stopPropagation();
            };

            document.querySelector('#ataca_'+i).onclick = function (event) {
                /* 
                *   EventListener (click del boton de ataque)
                *
                *   Realiza
                *       El jugador lanza un ataque.
                *       
                */
                event.preventDefault();
                let damage = players[i].launchAttack();
                let j = i == 0 ? 1 : 0;
                const card = document.querySelector('section#card_'+j);
                card.querySelector(".contenedorImpact").style.display="block";
                setTimeout(function(){ card.querySelector(".contenedorImpact").style.display="none"; }, 1000);
                let revi= players[j].reciveAttack(damage);
                view.pjUpdate(players[i], i);
                view.pjUpdate(players[j], j);
                event.stopPropagation();
            };

            document.querySelector('#defend_'+i).onclick = function (e) {
                /* 
                *   EventListener (click del boton de defensa)
                *
                *   Realiza
                *       El jugador se pone en modo defensa.
                *       
                */
                e.preventDefault();
                players[i].defend();
                view.pjUpdate(players[i], i);
                e.stopPropagation();
            };
        }
    }
}
