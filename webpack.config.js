const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const webpackConfig = {
    mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',

    entry: './assets/js/launch.js',

    output: {
        path: path.resolve(__dirname, 'dist'),
        filename:'main.js',
        publicPath: '/dist/'
    },

    module: {
        rules:[
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.scss$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
            },
            {
                test: /\.(png|jpg|jpeg|gif|ico|svg)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        publicPath: '/dist/', // se puede poner también en la configuración general de webpack
                        name: '[name]-[hash:6].[ext]'
                    }
                }]
            }
        ]
    },

    plugins: [
        new CopyWebpackPlugin(
            [
                {from: './assets/static', to: 'static'}
            ]
        ),
        new MiniCssExtractPlugin({
            filename: '[name].css',
        }),
        new CleanWebpackPlugin(),
    ],

    devtool: "inline-source-map",

    /*devServer: {
        contentBase: path.join(__dirname, ''),
    }*/
};

if (process.env.NODE_ENV === 'production') {
    webpackConfig.optimization = {
        minimizer: [new TerserPlugin()],
    };
}

module.exports = webpackConfig;